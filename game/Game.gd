extends Control

export var final_goal := 30
export var victory_scene: PackedScene
export var loss_scene: PackedScene
export(Array, PackedScene) var stage_scenes

var current_idx := 0
var current_stage: Stage
var current_party = null
var rows : int = 1
var cols : int = 3

func _ready():
	assert(load_next_stage())

func load_next_stage() -> bool:
	if current_stage != null:
		current_stage.queue_free()
	if current_idx >= stage_scenes.size():
		if $Budget.gold >= final_goal:
			# warning-ignore:return_value_discarded
			get_tree().change_scene_to(victory_scene)
		else:
			# warning-ignore:return_value_discarded
			get_tree().change_scene_to(loss_scene)
		return false
	current_stage = stage_scenes[current_idx].instance() as Stage
	current_stage.use_budget($Budget)
	# warning-ignore:return_value_discarded
	current_stage.connect("finished", self, "_on_stage_finished")
	current_idx += 1
	add_child(current_stage)
	
	$VBoxContainer/Debt.text = "Debt: %s" % Budget.display(final_goal)
	$VBoxContainer/Stage.text = "Stage: %d/%d" % [current_idx, stage_scenes.size()]
	return true

func increase_difficulty() -> void:
	if rows + 2 > cols:
		cols += 1
	else:
		rows += 1

# makes a new party of 4 members for the next level
func new_party() -> Array:
	var party = [
		{"Archer": 0},
		{"Berserker": 0},
		{"Healer": 0},
		{"Scholar": 0},
		{"Bard": 0},
	]
	party.shuffle()
	party.pop_back()
	return party

func _on_stage_finished():
	print(load_next_stage())
