class_name Budget extends Node

export var gold := 5.0

static func display(value: float) -> String:
	return "%d G" % (10 * int(value * 10))
