extends Control

export var game_scene: PackedScene

func _on_start_pressed():
	# warning-ignore:return_value_discarded
	get_tree().change_scene_to(game_scene)
