tool
class_name Card extends Control

export var block_scene: PackedScene setget _set_block_scene
export var dragged_card_scene: PackedScene
export var title_path := NodePath()
export var description_path := NodePath()
export var stats_path := NodePath()

var price := 0.0
var available := false

onready var enabled := true

func _ready():
	if block_scene != null:
		var block := block_scene.instance() as Block
		var title := get_node_or_null(title_path) as Label
		var description := get_node_or_null(description_path) as RichTextLabel
		var stats := get_node_or_null(stats_path) as Label
		title.text = block.block_name
		description.text = block.description
		stats.text = "%d sec. %s" % [block.base_duration, block.get_type_name()]
		$BlockView.texture = block.get_view_texture()
		price = block.price
		$Price.text = Budget.display(price)

func update_availability(budget_value: float):
	available = budget_value >= price
	var price_color = Color.white if available else Color.red
	$Price.modulate = price_color
	

func _on_mouse_entered():
	if enabled and available:
		$HoverSFX.play()
		$ToolTip.show()
		$Tween.interpolate_property(
			$BlockView, 'rect_position',
			$BlockView.rect_position, 20 * Vector2.UP,
			.2,
			Tween.TRANS_BACK, Tween.EASE_OUT
		)
		$Tween.start()

func _on_mouse_exited():
	if enabled and available:
		$ToolTip.hide()
		$Tween.interpolate_property(
			$BlockView, 'rect_position',
			$BlockView.rect_position, Vector2.ZERO,
			.2,
			Tween.TRANS_BACK, Tween.EASE_OUT
		)
		$Tween.start()

func get_drag_data(_position):
	if enabled and available:
		var dragged := dragged_card_scene.instance() as TextureRect
		dragged.texture = $BlockView.texture
		var preview := Control.new()
		preview.add_child(dragged)
		set_drag_preview(preview)
		return block_scene.instance()

func _set_block_scene(new_scene: PackedScene):
	block_scene = new_scene
	if block_scene != null:
		$BlockView.texture = block_scene.instance().get_view_texture()
