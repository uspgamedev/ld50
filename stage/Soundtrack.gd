class_name Soundtrack extends Node

enum Theme {
	BUILD_THEME_1, BUILD_THEME_2, BUILD_THEME_3, BUILD_THEME_4
}

export(Theme) var build_theme = Theme.BUILD_THEME_1

onready var currently_playing: AudioStreamPlayer = null

func play_build_sfx():
	$BuildSFX.play()

func play_hover_sfx():
	$HoverSFX.play()

func play_trash_sfx():
	$TrashSFX.play()

func play_build_theme():
	if currently_playing != null:
		currently_playing.stop()
	currently_playing = $BuildThemes.get_child(build_theme) as AudioStreamPlayer
	currently_playing.play()

func play_adventure_theme():
	if currently_playing != null:
		currently_playing.stop()
	currently_playing = $AdventureTheme
	currently_playing.play()
