extends Node

onready var distances: Array

func calculate_paths(map: Map):
	var queue := []
	
	distances.resize(map.height)
	for y in map.height:
		distances[y] = []
		distances[y].resize(map.width)
		for x in map.width:
			distances[y][x] = INF
			var position := Vector2(x, y)
			var block := map.get_block(position)
			if block != null and block.is_in_group('treasure'):
				distances[y][x] = 0
				queue.append(position)
	
	while not queue.empty():
		var position := queue.pop_front() as Vector2
		var current_distance := distance(position)
		for neighbor in map.find_neighbors(position):
			var distance := distance(neighbor)
			if current_distance + 1 < distance:
				distances[neighbor.y][neighbor.x] = current_distance + 1
				queue.append(neighbor)

func distance(position: Vector2) -> float:
	return distances[position.y][position.x]

func closest(pos1: Vector2, pos2: Vector2) -> bool:
	return distance(pos1) < distance(pos2)
