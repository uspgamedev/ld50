class_name Adventure extends Node2D

enum {
	READY, RUNNING, DONE
}

signal earned(amount)
signal finished()

export var gold_per_second := 1.0
export var time_speed := 1.0
export var party_scene: PackedScene

onready var state := READY
onready var visited := {}
onready var parties := {}

func _process(delta):
	if state == RUNNING:
		emit_signal("earned", delta * time_speed * gold_per_second)
		var party_count := 0
		for child in get_children():
			if child is Party:
				party_count += 1
		if party_count < 1:
			state = DONE
			emit_signal("finished")
		else:
			var party := parties.keys().front() as Party
			if $Timer.is_stopped():
				party.set_progress(null)
			else:
				party.set_progress(1.0 - $Timer.time_left / $Timer.wait_time)

func can_start() -> bool:
	return state == READY

func start(map: Map, party_members: Array = []):
	state = RUNNING
	var party := party_scene.instance() as Party
	add_child(party)
	for member in party_members:
		party.add_member(member)
	parties[party] = map.starting_position
	party.position = map.get_block(map.starting_position).get_screen_position()
	$TreasureFinder.calculate_paths(map)
	randomize()
	explore(map, party, map.starting_position)

func explore(map: Map, party: Party, position: Vector2) -> bool:
	print("on %s" % position)
	var block := map.get_block(position)
	var splitted := []
	# Expore the room
	if not _has_already_visited(position):
		_visit(position)
		block.visit()
		var duration := Rulebook.room_duration(map, party, position) as float
		print("exploring will take %s seconds" % duration)
		$Timer.start(duration / time_speed)
		yield($Timer, "timeout")
		block.clear()
		block.leave()
		print("finished exploring room")
		# Prepare to split party if necessary
		if block.split_count > 1:
			var splits := party.split_party(block.split_count)
			splitted = Fumbler.make_party_array(splits)
			for split_party in splitted:
				print("split at %s" % position)
				parties[split_party] = position
				split_party.position = map.get_block(position).get_screen_position()
				add_child(split_party)
	
	# Check for final destination
	if position == map.final_position:
		print("found exit at %s" % position)
		party.queue_free()
		return true
	
	# Decide next destination
	var neighbors := map.find_neighbors(position)
	neighbors.sort_custom($TreasureFinder, "closest")
	var best := []
	if neighbors.size() > 1:
		for i in neighbors.size():
			best.append(neighbors[i])
			if		i + 1 < neighbors.size() \
				and $TreasureFinder.closest(neighbors[i], neighbors[i + 1]):
					break
	best.shuffle()
	for i in best.size():
		neighbors[i] = best[i]
	for neighbor in neighbors:
		if not _has_already_visited(neighbor):
			print("going to %s" % neighbor)
			var next_block := map.get_block(neighbor)
			yield(_animate_movement(party, next_block.get_screen_position()),
				  "completed")
			
			if _join_with_other_party(party, neighbor):
				return true
			
			for split_party in splitted:
				_fork(map, split_party, position)
			
			# Otherwise explore the room themselves
			parties[party] = neighbor
			if yield(explore(map, party, neighbor), "completed"):
				return true
			
			# Then backtrack if the exit was not found
			print("backtracking to %s" % position)
			parties[party] = position
			yield(_animate_movement(party, block.get_screen_position()),
				  "completed")
			
			if _join_with_other_party(party, position):
				return true
	
	print("no exit found")
	return false

func _visit(position: Vector2):
	visited[position] = true

func _has_already_visited(position: Vector2) -> bool:
	return visited.get(position, false)

func _animate_movement(party: Party, target: Vector2):
	$Tween.interpolate_property(
		party, 'position',
		party.position, target,
		1.0 / time_speed,
		Tween.TRANS_CUBIC, Tween.EASE_IN_OUT
	)
	$Tween.start()
	yield($Tween, "tween_all_completed")

func _join_with_other_party(party: Party, position: Vector2) -> bool:
	for other in parties:
		if other != party and parties[other] == position:
			(other as Party).merge_party(party)
			# warning-ignore:return_value_discarded
			parties.erase(party)
			party.queue_free()
			print("parties merged at %s" % position)
			return true
	return false

func _fork(map: Map, party: Party, position: Vector2):
	print("forking party")
	yield(get_tree().create_timer(.1), "timeout")
	explore(map, party, position)
