extends TextureRect

signal block_removed(block)

func can_drop_data(_position, data):
	return data is Block

func drop_data(_position, block):
	if block is Block:
		emit_signal("block_removed", block)
