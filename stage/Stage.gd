class_name Stage extends Control

signal finished()

export(Array, Party.CharacterClass) var party_members
export var card_list_path := NodePath()
export var adventure_path := NodePath()
export var map_path := NodePath()
export var budget_display_path := NodePath()
export var hero_faces_path := NodePath()
export var action_path := NodePath()

var budget: Budget = null


func _ready():
	# warning-ignore:return_value_discarded
	get_map().connect("block_built", self, '_on_block_built')
	var faces := get_node_or_null(hero_faces_path)
	for i in party_members.size():
		var hero_class := party_members[i] as int
		var face := faces.get_child(i) as HeroFace
		face.set_hero(hero_class)
	$Soundtrack.play_build_theme()


func _process(_delta):
	var budget_display := get_node_or_null(budget_display_path) as Label
	budget_display.text = Budget.display(budget.gold)
	var card_list := get_node_or_null(card_list_path)
	for card in card_list.get_children():
		if card is Card:
			card.update_availability(budget.gold)
	var action := get_node_or_null(action_path) as ActionButton
	if get_map().validate():
		action.set_mode(ActionButton.PLAYABLE)
	else:
		action.set_mode(ActionButton.DISABLED)


func use_budget(the_budget: Budget):
	budget = the_budget


func get_map() -> Map:
	return get_node_or_null(map_path) as Map

func _on_block_built(block: Block):
	budget.gold -= block.price
	$Soundtrack.play_build_sfx()

func _on_block_removed(block):
	budget.gold += block.price
	$Soundtrack.play_trash_sfx()

func _on_play():
	var adventure := get_node(adventure_path) as Adventure
	if not adventure.can_start():
		return
	var map := get_map()
	if not map.validate():
		print("invalid map")
		return
	var card_list := get_node_or_null(card_list_path)
#	for card in card_list.get_children():
#		if card is Card:
#			card.enabled = false
#			card.mouse_default_cursor_shape = Control.CURSOR_FORBIDDEN
	$Soundtrack.play_adventure_theme()
	card_list.hide()
	$StartSuccessSFX.play()
	map.lock_blocks()
	adventure.start(map, party_members)


func _on_gold_earned(amount):
	budget.gold += amount


func _on_adventure_finished():
	$EndSFX.play()


func _on_EndSFX_finished():
	emit_signal("finished")
