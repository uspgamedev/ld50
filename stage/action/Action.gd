class_name ActionButton extends CenterContainer

enum {
	DISABLED, PLAYABLE, PLAYING
}

var cur_mode = DISABLED

signal play_pressed()

func set_mode(mode: int):
	cur_mode = mode
	match cur_mode:
		DISABLED:
			$PlayButton.self_modulate = Color(.2, .2 , .2)
			$PlayButton/Hint.show()
		PLAYABLE:
			$PlayButton.disabled = false
			$PlayButton.self_modulate = Color.white
			$PlayButton/Hint.hide()

func _on_play_pressed():
	print("entrei no play pressed")
	if cur_mode == DISABLED:
		$FailStartSFX.play()
	emit_signal("play_pressed")
