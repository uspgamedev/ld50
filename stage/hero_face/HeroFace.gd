class_name HeroFace extends CenterContainer

export(Array, Texture) var face_textures

func set_hero(hero_class: int):
	$TextureRect.show()
	$TextureRect.texture = face_textures[hero_class]
	$TextureRect/Badge/Name.text = Party.CLASS_NAME[hero_class]
