class_name Effect extends Object

enum Type {
	ROOM_DURATION,
	ROOM_CONNECTION
}

static func group_for(type: int) -> String:
	return "affects(%s)" % type
