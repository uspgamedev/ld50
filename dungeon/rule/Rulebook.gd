extends Node

func solve(effect: Dictionary) -> Dictionary:
	for rule in get_tree().get_nodes_in_group(Effect.group_for(effect['type'])):
		if rule is Rule:
			effect = rule._process_effect(effect)
	return effect

func room_duration(map: Map, party: Party, position: Vector2) -> float:
	var effect := solve({
		type = Effect.Type.ROOM_DURATION,
		map = map,
		party = party,
		position = position,
		value = 0.0
	})
	return effect['value'] as float

func room_connection(map: Map, position1: Vector2, position2: Vector2) -> bool:
	if position1.distance_to(position2) > 1:
		return false
	var effect := solve({
		type = Effect.Type.ROOM_CONNECTION,
		map = map,
		position1 = position1,
		position2 = position2,
		connected = true
	})
	return effect['connected'] as bool
