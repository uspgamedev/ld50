class_name Rule extends Node

export(Array, Effect.Type) var effect_types

func _ready():
	for type in effect_types:
		add_to_group(Effect.group_for(type))

func _process_effect(effect: Dictionary) -> Dictionary:
	return effect
