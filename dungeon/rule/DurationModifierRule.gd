tool
class_name DurationModifierRule extends Rule

export(Array, Party.CharacterClass) var affected_classes
export var modifier := 0.0

func _ready():
	effect_types = [
		Effect.Type.ROOM_DURATION
	]

func _process_effect(effect: Dictionary) -> Dictionary:
	var map := effect['map'] as Map
	var position := effect['position'] as Vector2
	var party := effect['party'] as Party
	var block := map.get_block(position)
	if self in block.get_children():
		for affected_class in affected_classes:
			if party.check_for_class(affected_class):
				effect['value'] += modifier
	return effect
