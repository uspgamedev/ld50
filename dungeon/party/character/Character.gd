class_name Character extends Sprite

var id : int

func _ready():
	$AnimationPlayer.play("default")
	$AnimationPlayer.seek(randf() * $AnimationPlayer.current_animation_length)

func set_character_class(t: Texture, c: int) -> void:
	self.texture = t
	id = c
