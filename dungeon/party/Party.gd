class_name Party extends Node2D


# A party is an array of Dictionaries, each one representing an
# adventurer.

enum CharacterClass {
	ARCHER, BERSERKER, HEALER, SCHOLAR, BARD
}

const CLASS_NAME := [
	"Archer", "Berserker", "Healer", "Scholar", "Bard"
]

export(Array, Texture) var sprite_texture
export var character_scene: PackedScene

var members : Array = []
var visited := {}
var current_positions = [
	Vector2(0,0),
	Vector2(0,0),
	Vector2(0,0),
	Vector2(0,0),
	Vector2(0,0),
	]

signal member_joined(member_class)
signal member_left(member_class)

onready var statuses := []

func _ready():
	current_positions = [
		$P41.position,
		$P42.position,
		$P43.position,
		$P44.position,
		$P11.position,
		]
	# warning-ignore:return_value_discarded
	connect("member_joined", self, "on_member_joined")
	# warning-ignore:return_value_discarded
	connect("member_left", self, "on_member_left")

func add_member(character_class: int) -> void:
	if members.size() >= 5:
		print("parties tem no max 5 membros")
		return
	members.push_back({ character_class: true })
	emit_signal("member_joined", character_class)


func on_member_joined(char_class) -> void:
	var new_member = character_scene.instance()
	add_child(new_member)
	new_member.set_character_class(sprite_texture[char_class], char_class)
	set_sprite_positions()

func remove_member() -> Dictionary:
	var leaver = members.pop_back()
	emit_signal("member_left", leaver.keys()[0])
	return leaver


func on_member_left(character_class) -> void:
	var chars = get_characters()
	var leaver_node = null
	for character in chars:
		if character.id == character_class:
			leaver_node = character
			break
	if leaver_node != null:
		leaver_node.queue_free()
		set_sprite_positions()


func has_members() -> bool:
	return members.size() > 0


func split_party(n: int) -> Array:
#	returns an array with the resulting sub parties
	assert(n == 2 or n == 3)
	members.shuffle()
	var SIZE = members.size()
	var subparties = []
	
	if SIZE <= n:
		while members.size() > 1:
			subparties.push_front([remove_member()])
	else:
		if n == 2:
			subparties.push_front([remove_member(), remove_member()])
		else:
			subparties.push_front([remove_member()])
			subparties.push_front([remove_member()])
	
	return subparties


func merge_party(new_party: Party) -> void:
	while new_party.has_members():
		var new_member = new_party.remove_member()
		add_member(new_member.keys()[0]) # testar isso melhor dps

func check_for_class(job: int) -> bool:

	for i in members:
		if i.has(job):
			return true
	return false


func get_characters() -> Array:
	var chars = []
	for child in get_children():
		if child is Character:
			chars.push_back(child)
	return chars

func set_progress(percent):
	if percent != null:
		$ProgressBar.show()
		$ProgressBar.value = percent * 100
	else:
		$ProgressBar.hide()

func set_sprite_positions() -> void:
	var size = members.size()
	match size:
		1:
			current_positions[0] = $P11.position
		2:
			current_positions[0] = $P21.position
			current_positions[1] = $P22.position
		3:
			current_positions[0] = $P31.position
			current_positions[1] = $P32.position
			current_positions[2] = $P33.position
		_:
			current_positions = [$P41.position, $P42.position,
					$P43.position, $P44.position, $P11.position]
	
	var characters = get_characters()
	for i in range(0, characters.size()):
		print(i)
		var character = characters[i]
		character.position = current_positions[i]
