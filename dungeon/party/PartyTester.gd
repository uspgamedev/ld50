extends Node2D

enum CharacterClass {
	ARCHER, BERSERKER, HEALER, SCHOLAR, BARD
}

onready var party = $Party

# Called when the node enters the scene tree for the first time.
func _ready():
	test_self()

func test_self() -> void:
	print("membros atuais: ", party.members)
	party.add_member(CharacterClass.BARD)
	party.add_member(CharacterClass.BERSERKER)
	party.add_member(CharacterClass.SCHOLAR)
	party.add_member(CharacterClass.HEALER)
	party.add_member(CharacterClass.ARCHER)
	print("membros atuais: ", party.members)
#	var split2 = party.split_party(3)
#	print("membros retirados: ", split2)
#	print("membros atuais: ", party.members)
#	for p in Fumbler.make_party_array(split1):
#		party.merge_party(p)
#	print("membros atuais: ", party.members)
#	for p in Fumbler.make_party_array(split2):
#		party.merge_party(p)
#	print("membros atuais: ", party.members)


func _on_Timer_timeout():
	var split1 = party.split_party(3)
	print("membros retirados: ", split1)
	print("membros atuais: ", party.members)
