class_name Fumbler extends Node


static func make_party_array(parties: Array) -> Array:
	var party_array = []
	for party in parties:
		var p = Party.new()
		p.members = party
		party_array.push_back(p)
	return party_array
