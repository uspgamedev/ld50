extends Rule

const UP = 1
const DOWN = 2
const LEFT = 4
const RIGHT = 8

const VEC2SIDE = {
	Vector2.UP: UP, Vector2.DOWN: DOWN,
	Vector2.LEFT: LEFT, Vector2.RIGHT: RIGHT
}

export(int, FLAGS, 'up', 'down', 'left', 'right') var disconnected_sides

func _process_effect(effect: Dictionary) -> Dictionary:
	var map := effect['map'] as Map
	var pos1 := effect['position1'] as Vector2
	var pos2 := effect['position2'] as Vector2
	var block1 := map.get_block(pos1) as Block
	var block2 := map.get_block(pos2) as Block
	if block1 == null or block2 == null:
		return effect
	var diff := Vector2.ZERO
	if self in block1.get_children():
		diff = pos2 - pos1
	elif self in block2.get_children():
		diff = pos1 - pos2
	else:
		return effect
	var side := VEC2SIDE.get(diff) as int
	if side & disconnected_sides:
		effect['connected'] = false
	return effect
