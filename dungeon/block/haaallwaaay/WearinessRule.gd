extends Rule

export var modifier := 0.0

func _process_effect(effect: Dictionary) -> Dictionary:
	var map := effect['map'] as Map
	var position := effect['position'] as Vector2
	var party := effect['party'] as Party
	var block := map.get_block(position)
	if self in block.get_children():
		party.statuses.append('weary')
	elif 'weary' in party.statuses:
		for neighbor in map.find_neighbors(position):
			var other_block := map.get_block(neighbor)
			if 	self in other_block.get_children() and \
				block.type == Block.Type.CHALLENGE:
					effect['value'] += modifier
					break
		party.statuses.erase('weary')
	return effect
