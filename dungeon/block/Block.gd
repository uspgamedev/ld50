tool
class_name Block extends MarginContainer

signal visited()
signal left()

enum Type {
	NONE,
	TERRAIN,
	CHALLENGE,
	DISTRACTION
}

const TYPE_NAMES := [
	"no type", "terrain", "challenge", "distraction"
]

export var block_name := "Block"
export var base_duration := 1.0
export var price := 1.0
export(Type) var type := Type.NONE
export(String, MULTILINE) var description := "Does stuff"
export var card_view: Texture
export(int, 1, 3) var split_count = 1

func get_screen_position() -> Vector2:
	var rect := get_global_rect()
	return rect.position + rect.size / 2

func get_view_texture() -> Texture:
	return card_view if card_view != null else $View.texture

func get_type_name() -> String:
	return TYPE_NAMES[type]

func clear():
	$View.hide()
	$Disabled.show()

func visit():
	emit_signal("visited")

func leave():
	emit_signal("left")
