extends Rule

func _process_effect(effect: Dictionary) -> Dictionary:
	var map := effect['map'] as Map
	var position := effect['position'] as Vector2
	var block := map.get_block(position)
	if self in block.get_children():
		effect['value'] += block.base_duration
	return effect
