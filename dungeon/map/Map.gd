tool
class_name Map extends GridContainer

signal block_built(block)
signal block_removed(block)

export var width := 3 setget _set_width
export var height := 3 setget _set_height
export var starting_position := Vector2.ZERO
export var final_position := Vector2.ZERO
export(Array, Resource) var fixed_placements: Array setget _set_fixed_placements

export var slot_scene: PackedScene

var blocks := {}
var prepared := false

func _ready():
	for fixed_placement in fixed_placements:
		if fixed_placement is FixedBlockPlacement:
			var block := fixed_placement.block_scene.instance() as Block
			var position := Vector2(fixed_placement.x, fixed_placement.y)
			blocks[position] = block
	_resize_grid()

func get_block(position: Vector2) -> Block:
	return blocks.get(position)

func find_neighbors(position: Vector2) -> Array:
	var result := []
	var positions := [
		position + Vector2.UP, position + Vector2.DOWN,
		position + Vector2.LEFT, position + Vector2.RIGHT
	]
	for neighbor in positions:
		var block := get_block(neighbor)
		if block and Rulebook.room_connection(self, position, neighbor):
			result.append(neighbor)
	return result

func put(position: Vector2, block: Control):
	if block in blocks.values():
		for slot in get_slots():
			if block in slot.get_children():
				slot.remove_child(block)
				# warning-ignore:return_value_discarded
				blocks.erase(slot.position)
				emit_signal("block_removed", block)
	var slot := get_child(_position_to_idx(position)) as Slot
	var old_block: Block
	for child in slot.get_children():
		if child is Block:
			old_block = child
			break
	if old_block != null:
		slot.remove_child(old_block)
		emit_signal("block_removed", old_block)
	blocks[position] = block
	emit_signal("block_built", block)
	call_deferred('_resize_grid')

func remove(block: Block):
	for slot in get_slots():
		if slot is Slot:
			if block in slot.get_children():
				slot.remove_child(block)
				# warning-ignore:return_value_discarded
				blocks.erase(slot.position)
				emit_signal("block_removed", block)
				block.queue_free()

func get_slots() -> Array:
	return get_children()

func get_total_slots() -> int:
	return width * height

func validate() -> bool:
	var filled := {}
	var queue := []
	queue.append(starting_position)
	while not queue.empty():
		var next := queue.pop_front() as Vector2
		filled[next] = true
		for neighbor in find_neighbors(next):
			if not filled.get(neighbor):
				queue.append(neighbor)
	return filled.get(final_position)

func lock_blocks():
	for slot in get_slots():
		if slot is Slot:
			slot.fixed = true

func _set_width(new_width):
	width = new_width
	_resize_grid()

func _set_height(new_height):
	height = new_height
	_resize_grid()

func _set_fixed_placements(new_placements: Array):
	while new_placements.size() > fixed_placements.size():
		var placement = new_placements[fixed_placements.size()]
		if placement == null:
			placement = FixedBlockPlacement.new()
		fixed_placements.append(placement)
	fixed_placements.resize(new_placements.size())

func _resize_grid():
	if not is_inside_tree(): return
	# Update slots
	if not prepared or Engine.editor_hint:
		columns = width
		while get_child_count() < get_total_slots():
			var slot := slot_scene.instance() as Slot
			# warning-ignore:return_value_discarded
			slot.connect("block_dropped", self, 'put')
			add_child(slot)
		while get_child_count() > get_total_slots():
			remove_child(get_children()[-1])
	# Update blocks
	for i in get_total_slots():
		var slot := get_child(i) as Slot
		var position := _idx_to_position(i)
		slot.position = position
		var block := blocks.get(position) as Node
		for current_block in slot.get_children():
			if current_block is Block and current_block != block:
				slot.remove_child(current_block)
	for i in get_total_slots():
		var slot := get_child(i) as Slot
		var block := blocks.get(_idx_to_position(i)) as Node
		if block != null and not block in slot.get_children():
			slot.fixed = not prepared
			slot.add_child(block)
	prepared = true

func _idx_to_position(i: int) -> Vector2:
	return Vector2(i % width, i / width)

func _position_to_idx(position: Vector2) -> int:
	return int(position.y * width + position.x)
