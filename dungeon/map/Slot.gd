tool
class_name Slot extends Control

signal block_dropped(position, block)

const INVALID_POSITION := -Vector2.ONE

export var dragged_card_scene: PackedScene

var fixed := false
var position := INVALID_POSITION

func can_drop_data(_position, data):
	return not fixed and data is Block

func drop_data(_position, block):
	if not fixed and block is Block:
		emit_signal("block_dropped", position, block)

func get_drag_data(_position):
	if fixed: return
	var block: Block
	for child in get_children():
		if child is Block:
			block = child
			break
	if block:
		var dragged := dragged_card_scene.instance() as TextureRect
		dragged.texture = block.get_view_texture()
		var preview := Control.new()
		preview.add_child(dragged)
		set_drag_preview(preview)
		return block
