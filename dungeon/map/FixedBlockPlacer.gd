class_name FixedBlockPlacement extends Resource

export(int, 0, 99) var x := 0
export(int, 0, 99) var y := 0

export var block_scene: PackedScene

func get_position() -> Vector2:
	return Vector2(x, y)
